<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class lead extends Model
{
    use HasFactory;
	protected $fillable = [
        'name', 'phone', 'email','remark','statusid','url','avatar'
    ];

    public function leadStatus()
	{
		return $this->belongsTo(lead_status::class, 'statusid');
	}

    public function cases()
	{
		return $this->hasMany(casemange::class);
	}
}
