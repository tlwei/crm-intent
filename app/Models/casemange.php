<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class casemange extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function caseStatus()
    {
        return $this->belongsTo(casestatus::class, 'casestatus');
    }

    public function lead()
    {
        return $this->belongsTo(lead::class,'lead_id','id');
    }

    public function items()
	{
		return $this->hasMany(caseitem::class,'case_id');
	}

}
