<?php

namespace App\Http\Controllers;

use App\Models\caseitem;
use Illuminate\Http\Request;
use Doctrine\DBAL\Types\Type;
use App\Models\CaseItemStatus;
use Illuminate\Support\Facades\DB;

class CaseitemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('caseitem.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        caseitem::create($request->validate([
            'case_id'=>'required|exists:casemanges,id',
            'task'=>'required',
            'orderdate'=>'required|date', 
            'casesitemstatus'=>'required'
        ]));
        
        

        return redirect("cases/{$request->case_id}/edit")->with('status', "item created successful");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\caseitem  $caseitem
     * @return \Illuminate\Http\Response
     */
    public function show(caseitem $item)
    {
       return $item; 

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\caseitem  $caseitem
     * @return \Illuminate\Http\Response
     */
    public function edit(caseitem $item)
    {
         $itemstatus = CaseItemStatus::all();
         return view("caseitem.edit",['item'=> $item,'itemstatus'=>$itemstatus]);
        //  return $item;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\caseitem  $caseitem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, caseitem $item)
    {
        // return $request->all();
        $item->update(
            $request->validate([
                'task'=>'required',
                'orderdate'=>'date',
                'executedate'=>'date',
                'casesitemstatus'=>'numeric',
            ])
        );
        return redirect("/cases/{$item->case_id}/edit")->with('status', "item updated successful");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\caseitem  $caseitem
     * @return \Illuminate\Http\Response
     */
    public function destroy(caseitem $item)
    {
        //
         
         $item->delete();
         
         return redirect("/cases/{$item->case_id}/edit")->with('status', "Item deleted successful");
        }

    public function bulkUpdate(Request $request){
        return DB::table('caseitems')->whereIn('id',$request->id)->update([
            'casesitemstatus'=>$request->status
        ]
        );
        // return $request->all();

        
    }
    public function bulkDelete(Request $request){
        return DB::table('caseitems')->whereIn('id',$request->id)->delete();
    }
}
