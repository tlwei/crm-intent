<?php

namespace App\Http\Controllers;

use App\Models\lead_status;
use Illuminate\Http\Request;

class LeadStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\lead_status  $lead_status
     * @return \Illuminate\Http\Response
     */
    public function show(lead_status $lead_status)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\lead_status  $lead_status
     * @return \Illuminate\Http\Response
     */
    public function edit(lead_status $lead_status)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\lead_status  $lead_status
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, lead_status $lead_status)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\lead_status  $lead_status
     * @return \Illuminate\Http\Response
     */
    public function destroy(lead_status $lead_status)
    {
        //
    }
}
