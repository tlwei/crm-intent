<?php

namespace App\Http\Controllers;

use App\Models\lead;
use App\Models\caseitem;
use App\Models\casemange;
use App\Models\casestatus;
use Illuminate\Http\Request;
use App\Models\CaseItemStatus;
use Illuminate\Support\Facades\DB;
use Database\Seeders\CasestatusSeeder;

class CasemangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    //    $casedata = casemange::withCount('items')->with('lead','caseStatus')->get();
       $casestatus = casestatus::all();
       $casedata = casemange::withCount('items')->with('lead','caseStatus')->paginate(10);
       
        return view('casemange.index',compact('casedata','casestatus'));
        // return $casedata;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('casemange.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  
        $request->validate([
            'name' => 'required',

        ]);
        $max = lead::count('id');

         Casemange::create([
            
            'lead_id'=>rand(1,$max),
            'casestatus'=>rand(1,2),
        ]
            
        );

        return redirect()->route('cases.index')->with('status','Case Create Sucessful!');
    }
        

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\casemange  $casemange
     * @return \Illuminate\Http\Response
     */
    public function show(casemange $case)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\casemange  $casemange
     * @return \Illuminate\Http\Response
     */
    public function edit(casemange $case)
    {
        

        $casestatus= casestatus::all();
        $itemstatus= CaseItemStatus::all();
        $items= caseitem::where('case_id',$case->id)->orderBy('created_at','desc')->paginate(5);
 
        // $items=["total"=>$items->total(),"rows"=>$items];
        // return $items;

        
        // $case->load('items');
        // return $case;
        
        return view('casemange.edit',['case'=>$case,'casestatus'=>$casestatus,'items'=>$items,'itemstatus'=>$itemstatus]) ;

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\casemange  $casemange
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, casemange $case)
    {
        //
        // return $request->all();
         $case->update([
            
            'casestatus'=>$request->statusid,
        ]);
        return redirect()->route('cases.index')->with('status', "Cases updated successful");

        // return $case;
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\casemange  $casemange
     * @return \Illuminate\Http\Response
     */
    public function destroy(casemange $case)
    {   
        $case->delete();
        
        // return $case;
        
        return redirect()->route('cases.index')->with('status',"The case $case->id has been deleted.");
       
        
    }

    public function getAllItem(casemange $case){
        $casestatus= casestatus::all();
        $itemstatus= CaseItemStatus::all();
        $items= caseitem::where('case_id',$case->id)->orderBy('created_at','desc')->paginate(5);
 
        // return $items=["total"=>$items->total(),"rows"=>$items];
    }

    public function getItemsById($id){
        return caseitem::where('case_id',$id)->orderBy('created_at','desc')->paginate(10);

    }

    public function casebulkUpdate(Request $request){
        return DB::table('casemanges')->whereIn('lead_id',$request->leadid)->update([
            'casestatus'=>$request->status
            ]
        );
        // return $request->all(); 

        
    }
    public function casebulkDelete(Request $request){
        return DB::table('casemanges')->whereIn('lead_id',$request->leadid)->delete();
    }
    
    public function caseChartData(){
        $select= DB::select("SELECT count(id) AS total,casestatus,substring(updated_at,1,7)AS 'month' FROM casemanges GROUP BY substring(updated_at,1,7),casestatus ORDER BY substring(updated_at,1,7);");
        $formattedData=[];
        $doing=[];
        $done=[];
        foreach ($select as $data) {
            # code...
            $month = date('M',strtotime($data->month));
            $formattedData[$month]['month']=$month;
            if ($data->casestatus == 1) {
                $label="doing";
                $doing[]=$data->total;
            }else{
                $label="done";
                $done[]=$data->total;
            }
            $formattedData[$month][$label]=$data->total;
        } 

        // return $formattedData;
        $keys=array_keys($formattedData);
        $data=array_values($formattedData);

        return ["labels"=>$keys,"data"=>$formattedData,"doing"=>$doing,"done"=>$done];
    }
    
}
 