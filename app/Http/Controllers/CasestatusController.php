<?php

namespace App\Http\Controllers;

use App\Models\casestatus;
use Illuminate\Http\Request;

class CasestatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\casestatus  $casestatus
     * @return \Illuminate\Http\Response
     */
    public function show(casestatus $casestatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\casestatus  $casestatus
     * @return \Illuminate\Http\Response
     */
    public function edit(casestatus $casestatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\casestatus  $casestatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, casestatus $casestatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\casestatus  $casestatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(casestatus $casestatus)
    {
        //
    }
}
