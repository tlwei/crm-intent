<?php

namespace App\Http\Controllers;

use App\Models\lead;
use App\Models\caseitem;
use App\Models\casemange;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //lead
        $totalLead= lead::count('id');
        //cases
        $totalDoneCase= casemange::where('casestatus',1)->count("id");
        $totalCase=casemange::count('id');
        $PDoneCase= round($totalDoneCase / $totalCase * 100);
        //items
        $totalPendingItem= caseitem::where('casesitemstatus',1)->count("*");
        $totalItem= caseitem::count("*");
        $PPendingItem= round($totalPendingItem/ $totalItem * 100);
        $CasemangeController=new CasemangeController;
        $caseChartData=$CasemangeController->caseChartData();
        
        return view('home',compact('totalLead','totalDoneCase','totalCase','PDoneCase','totalPendingItem','totalItem','PPendingItem','caseChartData'));
    }
}
