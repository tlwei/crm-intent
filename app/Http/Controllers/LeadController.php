<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\lead_status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        
        $alllead= Lead::count();
        
        $leadstatus=lead_status::all();
		$data = Lead::with('leadStatus',)->withCount('cases')->orderByDesc('updated_at')->orderByDesc('created_at');
        
		if ($request->has('search')) {
			
			$data = $data->where('name', 'like', "%{$request->search}%")
						   ->orWhere('email', 'like', "%$request->search%")
                           ->orWhere('phone', 'like', "%$request->search%")
						   ->paginate(10)
						   ->withQueryString();
			
			
		}else{
			$data = $data->paginate(10);
		}   
        
        
        $progress=($data->total()/$alllead)*100;
        
		
        
        return view('lead.index',compact('data','alllead','progress','leadstatus'));
        // return $data->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('lead.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required|unique:leads',
            'email' => 'required|unique:leads',
            
        ]);
        
        if($request->hasfile('avatar')){ 
            
            $path = $request->file('avatar')->store('images/avatar');
            
            Lead::create([
                'name'=>$request->name,
                'phone'=>$request->phone,
                'email'=>$request->email,
                'remark'=>$request->remark,
                'statusid'=>1,
                'url'=>$path,
                'avatar'=> $request->avatar     
                
            ]);
            return redirect()->route('lead.index')->with('status','Lead with pic add Sucessful!');

        }

        Lead::create([
            'name'=>$request->name,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'remark'=>$request->remark,
            'statusid'=>1,
              
        ]);
        return redirect()->route('lead.index')->with('status','Lead add Sucessful!');

       

    } 

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show(lead $lead)
    {
        return $lead->load(['leadStatus','cases']);
        return view('lead.show',compact('lead'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function edit(lead $lead)
    {
        $leadstatus= lead_status::all();
        return view('lead.edit',compact('lead','leadstatus')) ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, lead $lead)
    {
        $path = $lead->url;

        if($request->hasfile('avatar')&& $request->file('avatar')->getClientOriginalName()!= $lead->url){ 
            $path = $request->file('avatar')->storeAs('images/avatar',$request->file('avatar')->getClientOriginalName());
        }

        if(!$request->hasfile('avatar')&& !Storage::exists($lead->url)) $path=null;

        $vali=$request->validate([
            'name' => 'required',
            'phone' => 'required|unique:leads,phone,' .$lead->id,
            'email' => 'required|unique:leads,email,' .$lead->id,
            'remark'=>'nullable',
            'statusid'=>'numeric',
            'avatar'=>'nullable|mimes:jpg,jpeg,png'
        ]);
        

            $lead->update([
            'name'=>$request->name,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'remark'=>$request->remark,
            'statusid'=>$request->statusid,
            'url'=>$path,
            'avatar'=>isset($vali['avatar'])?$vali['avatar']:''
        ]);
        
        return redirect()->route('lead.index')->with('status','Lead Edit Sucessful!');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy(lead $lead)
    {
        $lead->delete();

        return redirect()->route('lead.index')->with('status',"The leads $lead->id has been deleted.");
        // return $lead;
    }

    public function leadbulkUpdate(Request $request){
        return DB::table('leads')->whereIn('id',$request->id)->update([
            'statusid'=>$request->status
            ]
        );
        // return $request->all();

        
    }
    public function leadbulkDelete(Request $request){
        return DB::table('leads')->whereIn('id',$request->id)->delete();
    }

}
