<?php

namespace App\Console\Commands;

use App\Models\casemange;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateCaseMangesStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateCaseMangesStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'test update case status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $maxx= casemange::count('id');
        foreach([3,2,1,]as$month){
            //rand days in month
           
            $max=cal_days_in_month(CAL_GREGORIAN,date('m',strtotime("-$month month")),date('Y'));
            // print(date("Y-m-d",strtotime("-$month months"))."\n");
            // print_r(range(1,rand(7,20)));
            for ($i=0; $i <rand(7,20) ; $i++) { 
                $arrdays[]=rand(1,$max);
            }

            $arrdays=array_unique($arrdays);
            sort($arrdays);

            
            foreach ($arrdays as $day) {
                $id=[];
                for ($i=0; $i < 5 ; $i++) { 
                    $id[]=rand(1,$maxx); 
                }

                $day=date('Y-m-d',strtotime("-$day days -$month month 2021"));
                $sql="update casemanges set created_at='$day',updated_at= '$day'where id in(".implode(",", array_unique($id)).")";
                print($sql);
                $updated=DB::update($sql);
                if ($updated) {
                    print("update $day success \n");
                }

            }
        }
    }
}
