
@extends('adminlte::page')

@section('title', 'create')

@section('content_header')
    <h1>add lead</h1>
    
@stop

@section('content')

{{-- <div class="embed-responsive embed-responsive-21by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
</div> --}}
<form action="/lead" method="POST" enctype="multipart/form-data" class="shadow p-3 mb-5 bg-white rounded">
    @csrf

    <div class="mb-3">
      <label for="avatar">Profile Picture</label><hr>
      <input type="file" id=avatar name="avatar" class="dropify" data-allowed-file-extensions="png jpg jpeg"/>
      <div class="alert alert-warning text-center" role="alert">
        Only PNG, JPG and JPEG files allowed !
      </div>
      <button type="button" class="btn btn-secondary" data-container="body" data-toggle="popover" data-placement="right" data-content="Only PNG, JPG and JPEG files allowed !">
        Help
        <span class="spinner-grow spinner-grow-sm text-danger" role="status" aria-hidden="true"></span>
        <span class="sr-only">Loading...</span>
      </button>
      
    </div>

    

    <div class="mb-3 ">
      <label for="name" class="form-label">name</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror shadow p-3 mb-5 bg-white rounded" id="name" name="name" placeholder="Please insert your name" value="{{ old('name') }}">
      
    </div>
    <div class="mb-3">
      <label for="phone" class="form-label">phone</label>
      <input type="text" class="form-control @error('phone') is-invalid @enderror shadow p-3 mb-5 bg-white rounded" id="phone" name="phone" placeholder="Please isnert your phone" value="{{ old('phone') }}">
    </div>

    <div class="mb-3">
        <label for="email" class="form-label">email address</label>
        <input type="email" class="form-control @error('email') is-invalid @enderror shadow p-3 mb-5 bg-white rounded" id="email" name="email" placeholder="Please insert your email" value="{{ old('email') }}">
        
    </div>

    <div class="mb-3">
        <label for="remark" class="form-label">Remark</label>
        <textarea type="remark" class="form-control shadow p-3 mb-5 bg-white rounded" id="remark" name="remark" rows="5">{{ old('remark') }}</textarea>
        
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection

@section('js')
  <script>
    $('.dropify').dropify().on('dropify.error.imageFormat', function(event, element){
        alert('Image format error message!');
    });

    $(function () {
      $('[data-toggle="popover"]').popover()
    })

    
  </script>
    
@endsection
