@extends('adminlte::page')

@section('title', 'edit')

@section('content_header')
    <h1>edit lead</h1>
@stop

@section('content')


<form action="/lead/{{ $lead->id }}" method="POST" enctype="multipart/form-data">
    @method('PUT')
    @csrf
     {{-- @if ($lead->url)
        <img src="/{{ $lead->url }}" class="rounded d-block" width="80" height="80">';
     @endif --}}
     <div class="mb-3">
        <label for="avatar">Profile Picture</label><hr>
        <input type="file" id=avatar name="avatar" class="dropify" data-allowed-file-extensions="png jpg jpeg" data-default-file="{{ $lead->url ? "/{$lead->url}" : 'https://www.google.com/search?q=profile+picture&rlz=1C1CHBF_enMY878MY878&sxsrf=ALeKk02wt1TCtbeAQFRh4oIBhekHHyGgcg:1624239951479&tbm=isch&source=iu&ictx=1&fir=ZbfgeaptF8Y5ZM%252CSmb2EEjVhvpzWM%252C_&vet=1&usg=AI4_-kTJU2_dnIUFXQeAai0bm1fPuz_Urg&sa=X&ved=2ahUKEwixytmqzafxAhUy4jgGHZGMCsIQ9QF6BAgWEAE#imgrc=ZbfgeaptF8Y5ZM' }}"/>
        <div class="alert alert-warning" role="alert">
          Only PNG, JPG and JPEG files allowed !
        </div>
        <button type="button" class="btn btn-secondary" data-container="body" data-toggle="popover" data-placement="right" data-content="Only PNG, JPG and JPEG files allowed !">
          Help
        </button>
      </div>
    
    <div class="mb-3">
      <label for="name" class="form-label">name</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Please insert your name" value="{{ old('name') ? old('name') : $lead->name }}">
      
    </div>
    <div class="mb-3">
      <label for="phone" class="form-label">phone</label>
      <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" placeholder="Please isnert your phone" value="{{ old('phone') ? old('phone') : $lead->phone }}">
    </div>

    <div class="mb-3">
        <label for="email" class="form-label">email address</label>
        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Please insert your email" value="{{ old('email') ? old('email') : $lead->email }}">
        
    </div>

    <div class="mb-3">
        <label for="remark" class="form-label">Remark</label>
        <textarea type="remark" class="form-control" id="remark" name="remark" rows="5">{{ old('remark') ? old('remark') : $lead->remark }}</textarea>
        
    </div>
    <div>
        <select class="form-select form-control" name="statusid" aria-label="Default select example">
            @foreach ($leadstatus as $status)
                <option value="{{ $status->id }}">{{ $status->name }}</option>
            @endforeach
            
        </select>
    </div>
    <br>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection

@section('js')
  <script>
    var $dropify = $('.dropify').dropify();
    $dropify.on('dropify.error.imageFormat', function(event, element){
        alert('Image format error message!');
    });

    $dropify.on('dropify.afterClear', function(event, element){
        alert('File deleted');
    });
    
    $(function () {
      $('[data-toggle="popover"]').popover()
    })
  </script>
    
@endsection