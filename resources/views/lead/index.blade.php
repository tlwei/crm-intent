@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    
@stop

@section('content')
<div class="container ">
	<div class="row">
		<div>
			<x-adminlte-info-box title="Total lead" text="{{ $data->total() }}" icon="fas fa-tasks"
			theme="info"
			progress="{{ $progress }}"
			progresstheme="dark"
			description="{{ $data->total()}}/{{ $alllead }}"
			draggable="true"
			/>
		</div>
		<div class="col-md-12">
			<div class="card mb-3 ">
				<div class="card-header mt-2">
					<div class="col-2"><h3>Leads<span class="badge badge-secondary ml-2">Info</h3></div>
				</div>
				<div class="card-body table-responsive">
	
					<div class="row">
						<div class="col-12">
							<div class="table-responsive">
								<div id="toolbar">
									<div class="form-inline" role="form">
										 <div class="form-group">
											<a href="/lead/create" type="button" class="btnbtn btn-success btn-lg ">Add lead</a> 								 
											<div class="btn-group mr-2"> 
												@foreach ($leadstatus as $status)
													<button id="show" class="btn btn-outline-primary" onclick="leadbulkUpdate({{ $status->id }})">{{$status->name}}</button>
												@endforeach
												<button  class="btn btn-danger" onclick="leadbulkDelete()">Multi Delete</button>
											</div>
										  </div>
									</div>
								</div>
								<table 
										class="table table-striped table-bordered "
										id="leadtable"
										data-toggle="table" 
										data-search="true"
										{{-- data-url="/api/getItemById/{{ $case->id }}" --}}
										{{-- data-side-pagination="server" --}}
										data-pagination="true"
										data-server-sort="false"
										data-show-export="true"
										data-export-data-type="all"
										data-query-params="queryParams"
										{{-- data-sticky-header="true" --}}
										data-height="600"
										data-toolbar="#toolbar"
										data-click-to-select="true"
										data-show-print="true"
										data-use-pipeline="true"
										data-pipeline-size="500"
										data-show-columns="true"
										data-show-columns-search="true"
										data-show-columns-toggle-all="true"
										data-smart-display="true"
										data-thead-classes="thead-light"
										data-filter-control="true"
										data-show-search-clear-button="true"
										data-server-sort="false"

									>
								
									<thead>
										<th data-checkbox="true"></th>
										<th data-sortable="true" data-field="url" data-formatter="AvatarFormatter">Image</th>
										<th data-sortable="true" data-field="id">Lead id</th>
										<th data-sortable="true" data-field="name" data-filter-control="input">Name</th>
										<th data-sortable="true" data-field="phone" data-filter-control="input">Phone</th>
										<th data-sortable="true" data-field="email" data-filter-control="input">Email</th>
										<th data-sortable="true" data-field="remark">Remark</th>
										<th data-sortable="true" data-field="cases">Cases</th>
										<th data-sortable="true" data-field="status" data-filter-control="input">Status</th>
										<th data-sortable="true" data-field="created_at">Created at</th>
										<th data-sortable="true" data-field="updated_at">Updated at</th>
										<th>Edit</th>
										<th>Delete</th>
									</thead>
									
									<tbody>
										@foreach ($data as $lead)
										<tr> 
										<td></td>
										<td>{{ $lead->url}}</td>
										<td>{{ $lead->id}}</td>
										<td>{{ $lead->name}}</td>
										<td>{{ $lead->phone}}</td>
										<td>{{ $lead->email}}</td>
										<td>{{ $lead->remark}}</td>
										<td>
											
											<button onclick="showcase()">
											{{ $lead->cases_count }}
											</button>
											
										</td>
										<td>{{ $lead->leadStatus->name}}</td>
										<td>{{ $lead->created_at}}</td>
										<td>{{ $lead->updated_at}}</td>
										<td><a href="/lead/{{ $lead->id }}/edit " type="button" class="btn btn-warning" >edit</a></td>
										<td>
										<form action="/lead/{{ $lead->id }}" method="POST">
											@method('DELETE')
											@csrf
											<button type="submit" class="btn btn-danger">delete</button>
										</form>
										</td>
										
										
										</tr>
									@endforeach
									</tbody>
									
								</table>
							</div>
						</div>
					</div>
					<br>
					<div class="float-right">
						{{ $data->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> 

    var $table = $('#leadtable')

	function leadbulkUpdate(status){
        
		var id=[];
		$table.bootstrapTable('getSelections').map((value,index)=>{
			id.push(value.id);
		})

		$.post("/api/leadbulkUpdate",{id:id,status:status},function(data,status){
			// alert("DATA:"+data+"\nStatus:"+status);
			location.reload();
		});
		
	  
	  }

  function leadbulkDelete(){
	  
		var id=[];
		$table.bootstrapTable('getSelections').map((value,index)=>{
			id.push(value.id);
		})

		$.post("/api/leadbulkDelete",{id:id},function(data,status){
			location.reload();
		});
	  
	  }

	  function AvatarFormatter(value){
			return value && '<img src="'+value+'" class="rounded mx-auto d-block" width="80" height="auto">';
	  }

	
	</script>
	
@stop