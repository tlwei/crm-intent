@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Dashboard</h1>
@stop

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

@section('content')

    <div class="row">
        <div class="col-4">
            <x-adminlte-info-box 
                title="Leads" 
                text="{{ $totalLead }}" 
                icon="text-orange" 
                icon-theme="dark" 
                progress-theme="dark"
                />
        </div>
        <div class="col-4">
            <x-adminlte-info-box 
                title="Cases" 
                :text="sprintf('%d/%d',$totalDoneCase,$totalCase)" 
                icon="fas fa-clipboard text-orange" 
                icon-theme="dark" 
                progress="{{ $PDoneCase }}" 
                progress-theme="dark"
                description="{{ $PDoneCase }} % of the tasks have been done"/>
        </div>
        <div class="col-4">
            <x-adminlte-info-box 
                title="Items" 
                :text="sprintf('%d/%d',$totalPendingItem,$totalItem)" 
                icon="fas fa-balance-scale text-orange" 
                icon-theme="dark" 
                progress="{{ $PPendingItem }}" 
                progress-theme="dark"
                description="{{ $PPendingItem }} % of the tasks are pending"/>
        </div>
    </div>

    <div class="row">
        {{-- <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <canvas id="caseChart">asd</canvas> 
                </div>
            </div>
        </div> --}}
        {{-- <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div id="g2-chart"></div>
                </div>
            </div>
        </div> --}}
    </div>
    
@stop

@section('js')

    <script>
    
    
    // const labels ={!!json_encode($caseChartData['labels'])!!};
    
    // const data = {
    //     labels: labels,
    //     datasets: [{
    //         label: 'Doing',
    //         backgroundColor: 'rgb(255, 99, 132)',
    //         borderColor: 'rgb(255, 99, 132)',
            
    //         data:@json($caseChartData['doing']),
    //         parsing:{
    //             yAxiskey:"doing",     
    //         }
    //     },{
    //         label: 'Done',
    //         backgroundColor: 'rgb(25, 99, 132)',
    //         borderColor: 'rgb(255, 99, 132)',
    //         data: @json($caseChartData['done']),
    //         parsing:{
    //             yAxiskey:"done",     
    //         } 
    //     }]
    // };

    // const config = {
    //     type: 'bar',
    //     data,
    //     options: {}
    // };

    // //display
    // var caseChart = new Chart(
    //     document.getElementById('caseChart'),
    //     config
    // );

    const data = [
        { item: 'Jan', count: 40, percent: 0.4 },
        { item: 'Feb', count: 21, percent: 0.21 },
        { item: 'Mac', count: 17, percent: 0.17 },
        { item: 'April', count: 13, percent: 0.13 },
        { item: 'May', count: 9, percent: 0.09 },
    ];  

    const chart = new G2.Chart({
        container: 'g2-chart',
        autoFit: true,
        height: 500,
    });

    chart.data(data);

    chart.coordinate('theta', {
        radius: 0.85
    });

    chart.scale('percent', {
        formatter: (val) => {
            val = val * 100 + '%';
        return val;
        },
    });
    chart.tooltip({
        showTitle: false,
        showMarkers: false,
    });
    chart.axis(false); // 关闭坐标轴
    const interval = chart
        .interval()
        .adjust('stack')
        .position('percent')
        .color('item')
        .label('percent', {
            offset: -40,
            style: {
            textAlign: 'center',
            shadowBlur: 2,
            shadowColor: 'rgba(0, 0, 0, .45)',
            fill: '#fff',
            },
        })
        .tooltip('item*percent', (item, percent) => {
            percent = percent * 100 + '%';
            return {
                name: item,
                value: percent,
            };
        })
        .style({
            lineWidth: 1,
            stroke: '#fff',
        });
    chart.interaction('element-single-selected');
    chart.render();

// 默认选择
interval.elements[0].setState('selected', true);
</script>
    
@endsection
