@extends('adminlte::page')

@section('title', 'edit item')

@section('content_header')
    <h1>edit item | {{ $item->id }}</h1>
@stop

@section('content')

<div class="container">
    <div class="card">
        <div class="card-body">
            <form action="/item/{{ $item->id }}" method="POST">
                @method('PUT')
                @csrf
                <div class="mb-3">
                    <label for="task" class="form-label">Task</label>
                    <input type="text" class="form-control" id="task" name="task" value="{{$item->task}}">
                </div>

                <div class="mb-3">
                  <label for="orderdate" class="form-label">Order Date</label>
                  <input type="datetime-local" class="form-control" id="orderdate" name="orderdate" value="{{ Carbon\Carbon::parse($item->orderdate)->toDateTimeLocalString()}}" >
                </div>

                <div class="mb-3">
                  <label for="executedate" class="form-label">Execute Date</label>
                  <input type="datetime-local" class="form-control" id="executedate" name="executedate" value="{{ Carbon\Carbon::parse($item->executedate)->toDateTimeLocalString()}}">                 
                </div>

                <div class="mb-3">
                  <label for="executedate" class="form-label">Item Status</label>
                  <select class="form-select form-control" name="casesitemstatus" aria-label="Default select example">
                      @foreach ($itemstatus as $status)
                          <option value="{{ $status->id }}">{{ $status->name }}</option>
                      @endforeach    
                  </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
              <br>
        </div>
    </div>
</div>
    
@endsection