@extends('adminlte::page')

@section('title', 'create item')

@section('content_header')
    <h1>add item</h1>
@stop

@section('content')



    <div class="container">
        <div class="card">
            <div class="cardbody">
                <form action="/item" method="post">
                    @csrf
                    <input type="hidden" name="case_id" value="{{request('case_id')}}">
                    <input type="hidden" name="casesitemstatus" value="3">
                        <div class="mb-3">
                            <label for="task" class="form-label">task</label>
                            <input type="text" class="form-control" id="task" name="task">
                            
                        </div>
                        <div class="mb-3">
                            <label for="phone" class="form-label">Order Date</label>
                            <input type="datetime-local" class="form-control" id="orderdate" name="orderdate">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

@endsection