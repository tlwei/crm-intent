@extends('adminlte::page')

@section('title', 'casemanage')

@section('content_header')
    
@stop

@section('content')
<div class="container">
	<div class="col-md-12">
		<div class="card" style="height: 80vh">
			<div class="card-header row">
				<div class="col-3"><h1>Case<span class="badge badge-secondary ml-1">Info</h1></div>	
			</div>
			<div class="card-body table-responsive">
				<div id="toolbar">
					<div class="form-inline" role="form">
						<div class="form-group">
							<a href="/cases/create" type="button" class="btnbtn btn-success btn-lg">Add case</a> 
							<div class="btn-group mr-2"> 
								@foreach ($casestatus as $status)
									<button id="show" class="btn btn-outline-primary" onclick="casebulkUpdate({{$status->id}})">{{$status->name}}</button>
								@endforeach
								<button  class="btn btn-danger" onclick="casebulkDelete()">Multi Delete</button>
							</div>
						</div>
					</div>
				</div>

					<table 
					
							class="table table-striped table-bordered "
							id="casetable"
							data-toggle="table" 
							data-search="true"
							{{-- data-url="/api/getItemById/{{ $case->id }}" --}}
							{{-- data-side-pagination="server" --}}
							data-pagination="true"
							data-server-sort="false"
							data-show-export="true"
							data-export-data-type="all"
							data-query-params="queryParams"
							{{-- data-sticky-header="true"
							data-sticky-header-offset-y="120" --}}
							data-toolbar="#toolbar"
							data-click-to-select="true">

						<thead>
							<th data-checkbox="true"></th>
							<th data-sortable="true" data-field="lead_id">Lead Id</th>
							<th data-sortable="true" data-field="lead_name">Lead Name</th>
							<th data-sortable="true" data-field="item">Item</th>
                            <th data-sortable="true" data-field="casestatus">casestatus</th>
							<th data-sortable="true" data-field="created_at">Created at</th>
							<th data-sortable="true" data-field="updated_at">Updated at</th>
							<th>Edit</th>
							<th>Delete</th>
						</thead>
                        
                        <tbody>
							@foreach ($casedata as $item)
							<tr>
							<td></td>
							<td>{{ $item->lead_id }}</td>
							<td>
								@if ($item->lead)
									{{ $item->lead->name}}
								@else
								@endif	
							</td>
							<td>{{ $item->items_count }}</td>
							<td>{{ $item->caseStatus->name}}</td>
							<td>{{ $item->created_at}}</td>
                            <td>{{ $item->updated_at }}</td>
							<td><a href="/cases/{{ $item->id}}/edit " type="button" class="btn btn-warning" >edit</a></td>
							<td>
							<form action="/cases/{{ $item->id}}" method="POST">
								@method('DELETE')
								@csrf
								<button type="submit" class="btn btn-danger">delete</button>
							</form>
							</td>
						</tr>
							@endforeach
                        </tbody>
						
					</table>

			</div>
		</div>
		<div class="float-right">
			{{$casedata->links()}}
		</div>
	</div>
		<br>
		<br>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> 
		var $table = $('#casetable')
		function casebulkUpdate(status){
			// console.log(status);
			var leadid=[];
			$table.bootstrapTable('getSelections').map((value,index)=>{
				// console.log(value);
				leadid.push(value.lead_id);
				// console.log(id);
		})
			

			$.post("/api/casebulkUpdate",{leadid:leadid,status:status},function(data,status){
				// console.log(data);
				// alert("DATA:"+data+"\nStatus:"+status);
				location.reload();
				$table.bootstrapTable(refresh);
			});
				
	  
	  }

 		 function casebulkDelete(){
	  
			var leadid=[];
			$table.bootstrapTable('getSelections').map((value,index)=>{
				leadid.push(value.lead_id);
			})

		$.post("/api/casebulkDelete",{leadid:leadid},function(data,status){
			location.reload();
		});
	  
	  }
	
	</script>
@stop