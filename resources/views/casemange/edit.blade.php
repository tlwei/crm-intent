@extends('adminlte::page')

@section('title', 'editcase')

@section('content_header')
    <h1>edit case | {{$case->id }}</h1>
@stop

@section('content')



<div class="col-md-12">
  <div class="card">
    <div class="card-header">
      <form action="/cases/{{ $case->id }}" method="POST">
        @method('PUT')
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Lead id</label>
            <input type="text" class="form-control" id="lead_id" name="lead_id"  value="{{ old('lead_id') ? old('lead_id') : $case->lead_id }}" readonly="true">
        </div>

        <div class="mb-3">
          <label for="name" class="form-label">Lead Name</label>
          <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name"  value="{{ old('name') ? old('name') : $case->lead->name }}" readonly='true'>
        </div>
        <div>
            <label for="casestatus" class="form-label">Case Status</label>
            <select class="form-select form-control" name="statusid" aria-label="Default select example">
                @foreach ($casestatus as $status)
                    <option value="{{ $status->id }}">{{ $status->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>

    <br>

  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h2>Items<span class="badge badge-secondary ml-2">Info</h2>
        <div id="toolbar">
          <div class="form-inline" role="form">
            <div class="form-group">
              <a href="/item/create?case_id={{ $case->id }}" class="btn btn-primary">Add</a>
                
              <div class="btn-group mr-2"> 
                  @foreach ($itemstatus as $status)
                      <button id="show" class="btn btn-outline-primary" onclick="bulkUpdate({{$status->id}})">{{$status->name}}</button>
                  @endforeach
                  <button  class="btn btn-danger" onclick="bulkDelete()">Multi Delete</button>
              </div>
            </div>
          </div>
        </div>
            
              <table class="table table-border table-striped" 
                    id="table"
                    data-toggle="table" 
                    data-search="true"
                    {{-- data-url="/api/getAllItem/{{ $case->id }}"
                    data-side-pagination="server"  --}}
                    
                    data-pagination="true"
                    data-server-sort="false"
                    data-show-export="true"
                    data-export-data-type="all"
                    data-query-params="queryParams"
                    data-sticky-header="true"
                    data-toolbar="#toolbar"
                    data-click-to-select="true"
                    >
                  <thead>
                      <tr>
                        <th data-checkbox="true"></th>
                        <th data-sortable="true" data-field="id">ID</th>
                        <th data-sortable="true" data-field="task">Task</th>
                        <th data-sortable="true" data-field="orderdate">Order Date</th>
                        <th data-sortable="true" data-field="executedate">Execute Date</th> 
                        <th data-sortable="true" data-field="status">Status</th>
                        <th data-sortable="true" data-field="created_at">Created At</th>
                        <th data-sortable="true" data-field="updated_at">Updated At</th>
                        <th>edit</th>
                        <th>delete</th>
                      </tr>
                  </thead>
                  <tbody>
                        @foreach($items as $item)
                            <tr>
                                <td></td>
                                <td>{{$item->id}}</td>
                                <td>{{$item->task}}</td>
                                <td>{{$item->orderdate}}</td>
                                <td>{{$item->executedate}}</td>
                                <td>{{$item->status->name}}</td>
                                <td>{{$item->created_at}}</td>
                                <td>{{$item->updated_at}}</td>
                                <td><a href="/item/{{$item->id}}/edit" type="button" class="btn btn-warning">edit</a></td>
                                
                                <td>
                                   <form action="/item/{{ $item->id }}" method="POST">
                                      @method('DELETE')
                                      @csrf
                                       <button type="submit" class="btn btn-danger">Delete</button>
                                    </form> 
                                    {{-- <button type="submit" class="btn btn-danger" onclick="mydelete('{{ $item->id }}')">Delete</button> --}}
                                </td>
                            </tr>
                        @endforeach
                  </tbody>

              </table>
              <div class="float-right">
                {{$items->links()}}
              </div>
        </div>
      </div>
    </div>

@stop

@section('js')
<script>
    var $table = $('#table')

    function bulkUpdate(status){
        
          var id=[];
          $table.bootstrapTable('getSelections').map((value,index)=>{
              id.push(value.id);
          })

          $.post("/api/bulkUpdate",{id:id,status:status},function(data,status){
            // console.log(data);
            // alert("DATA:"+data+"\nStatus:"+status);
              location.reload();
          });
          
        
        }

    function bulkDelete(){
        
          var  id=[];
          $table.bootstrapTable('getSelections').map((value,index)=>{
              id.push(value.id);
          })

          $.post("/api/bulkDelete",{id:id},function(data,status){
              // alert("Delete:"+data+"\nStatus:"+status);
              location.reload();
          });
        
        }
      // function mydelete(id){

      //   var success = "";
      //   var post = $.ajax({
      //       type: "DELETE",
      //       url: "/item/"+id,
           
      //       success: function(res){
      //         console.log(res);
      //         if(res){
      //           alert("deleted") 
      //           location.reload();
      //         }
      //       },
            
      //     })
      // }
  </script>
    
@stop

