@extends('adminlte::page')

@section('title', 'createcase')

@section('content_header')
    <h1>add case</h1>
@stop

@section('content')



<form action="/cases" method="POST">
  @csrf
    <div class="mb-3">
    <label for="name" class="form-label">Lead name</label>
    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Please insert your name" value="{{ old('name') }}">
    <br>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
