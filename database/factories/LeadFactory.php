<?php

namespace Database\Factories;

use App\Models\lead;
use Illuminate\Database\Eloquent\Factories\Factory;

class LeadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = lead::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=>$this->faker->name(),
            'phone'=>$this->faker->e164PhoneNumber(),
            'email'=>$this->faker->freeEmail(),
            'remark'=>$this->faker->sentence(),
            'statusid'=>1,

            //
        ];
    }
}
