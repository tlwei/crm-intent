<?php

namespace Database\Factories;

use App\Models\casestatus;
use Illuminate\Database\Eloquent\Factories\Factory;

class CasestatusFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = casestatus::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
