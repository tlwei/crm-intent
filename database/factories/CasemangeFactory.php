<?php

namespace Database\Factories;

use App\Models\casemange;
use Illuminate\Database\Eloquent\Factories\Factory;

class CasemangeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = casemange::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
