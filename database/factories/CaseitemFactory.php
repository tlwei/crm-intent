<?php

namespace Database\Factories;

use App\Models\caseitem;
use Illuminate\Database\Eloquent\Factories\Factory;
Use App\Models\casemange;

class CaseitemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = caseitem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $items = ['Advertisting','Video shooting','Video editing','market survey','product saling'];
		$date = $this->faker->dateTimeBetween('-100 days', 'now', 'Asia/Kuala_Lumpur')->format('Y-m-d H:i:s');
		$days = rand(1, 14);
		$hours = rand(1, 12);
		$datea = date('Y-m-d H:i:s', strtotime("$date +$days days +$hours hours"));
        $max = casemange::count('id');
		
		
        return [
            
            'task'=> $items[rand(0, 4)],
			'orderdate'=> $date,
			'executedate'=> $datea,
			'casesitemstatus'=>rand(1,3),
            'case_id'=>rand(1,$max)
        ];
    }
}
