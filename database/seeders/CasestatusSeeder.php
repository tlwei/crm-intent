<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\casestatus;

class CasestatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $casestatuses= ['doing','done',];
        foreach ($casestatuses as $casestatus){
            casestatus::create([
                'name'=>$casestatus
            ]);
    }
}
}
