<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\casestatus;
use App\Models\caseitem;


class CaseitemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        caseitem::factory()->count(50)->create();

        }
    
}
