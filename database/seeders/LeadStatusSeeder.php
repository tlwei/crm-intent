<?php

namespace Database\Seeders;
Use App\Models\lead_status;

use Illuminate\Database\Seeder;

class LeadStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses= ['active','inactive','pending'];
        foreach ($statuses as $status){
            lead_status::create([
                'name'=>$status
            ]);
        }        
    }
}
