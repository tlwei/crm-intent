<?php

namespace Database\Seeders;

use App\Models\CaseItemStatus;
use Illuminate\Database\Seeder;


class CaseItemStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$caseitemstatuses = ['pending','doing','done'];
		foreach ($caseitemstatuses as $status) {
			CaseItemStatus::create([
				'name'=>$status
			]);
		}
    }
}