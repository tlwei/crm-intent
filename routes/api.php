<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LeadController;
use App\Http\Controllers\CaseitemController;
use App\Http\Controllers\CasemangeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('/lead', LeadController::class);
Route::resource('/cases', CasemangeController::class);
Route::resource('/item', CaseitemController::class);

Route::get('/getItemById/{id}',[CasemangeController::class,'getItemsById']);
Route::post('/bulkUpdate',[CaseitemController::class,'bulkUpdate']); 
Route::post('/bulkDelete',[CaseitemController::class,'bulkDelete']); 
Route::post('/leadbulkUpdate',[LeadController::class,'leadbulkUpdate']); 
Route::post('/leadbulkDelete',[LeadController::class,'leadbulkDelete']); 
Route::post('/casebulkUpdate',[CasemangeController::class,'casebulkUpdate']); 
Route::post('/casebulkDelete',[CasemangeController::class,'casebulkDelete']); 
Route::get('/getAllItem',[CasemangeController::class,'getAllItem']); 
Route::get('/showcase',[LeadController::class,'showcase']); 
Route::get('/caseChartData',[CasemangeController::class,'caseChartData']); 
