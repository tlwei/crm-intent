<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LeadController;
use App\Http\Controllers\CasemangeController;
use App\Http\Controllers\CaseitemController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
// Route::get('/', function () { return view('home');});
Route::resource('/', HomeController::class);
Route::resource('/lead', LeadController::class);
Route::resource('/cases', CasemangeController::class);
Route::resource('/item', CaseitemController::class);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

